/**
 * Mampu menggabungkan konsep Conditional dan Iteration dalam kasus sederhana
 * Buat sebuah function yang bisa menerima parameter yang bisa melengkapi for loop.
 * function yang dibuat hanya bisa menerima tipe data number
 */

console.log("-----------------------------------------");
console.log("Menentukan bilangan ganjil & genap:");
const param = (input) => {
  if(typeof input === "number"){
      for(let i=1; i<=input; i++){
          if(i%2==1){
              console.log(`${i}: Adalah bilangan ganjil`);
          }else{
              console.log(`${i}: Adalah bilangan genap`);
          }
      }
  }else{
      console.log("Tolong Input tipe data number!!!");
  }
}
// param(10);

console.log("-----------------------------------------");
console.log("Kelipatan 3 dengan pertambahan index 2:");
 const param1 = (input) => {
  if(typeof input === "number"){
      for(let i=1; i<=input; i++){
          if(i%3===0){
              console.log(`${i}: Kelipatan 3`);
          }else{
              console.log(i);
          }
      }   
  }else{
      console.log("Tolong Input tipe data number!!!");
  }
}
// param1(10);
console.log("-----------------------------------------");
console.log("Menentukan faktor prima:");
const primeFactors = (number, result = []) => {
    let divisor = 2;
        if(number <= 1){
            result = [number];
          }else{
            while (number >= 2) {
              if (number % divisor == 0) {
                result.push(divisor);
                number /= divisor;
              } else {
                divisor++;
              }
            }
        }
        return result;
    }
var input = 50;
console.log(`Faktor prima dari ${input} adalah ${primeFactors(input)}`);